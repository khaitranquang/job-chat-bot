#!/bin/sh

install_packages() {
    if ! which python3.6 &>/dev/null; then
        add-apt-repository -y ppa:deadsnakes/ppa
    fi

    apt-get update -qq && apt-get install -y python3.6 python3.6-dev python3-pip libmysqlclient-dev python-dev nginx
    python3.6 -m pip install virtualenv
    echo "[+] Installed packages"
}

make_dir() {
    for path in /var/log/chatbot/api/nginx/ /var/log/chatbot/api/app/ /var/log/chatbot/api/gunicorn/ /etc/chatbot/api/ /usr/lib/chatbot/api/src;do
        mkdir -p $path
    done
    if [ ! -f /usr/lib/chatbot/api/projectenv/bin/python ];
        then python3.6 -m virtualenv /usr/lib/chatbot/api/projectenv;
    fi
    echo "[+] Created directories"
}

install_python_libs() {
    /usr/lib/chatbot/api/projectenv/bin/pip install --upgrade pip
    /usr/lib/chatbot/api/projectenv/bin/pip install -r requirements.txt
    echo "[+] Installed python libraries"
}

copy_source_code() {
    rsync -avr --delete src/ /usr/lib/chatbot/api/src/
    echo "[+] Copied source code"
}

config() {
    cp deploy/config/chatbot-api.service /lib/systemd/system/
    cp deploy/config/api.khaitq.tk /etc/chatbot/api/api.khaitq.tk 
        ln -sf /etc/chatbot/api/api.khaitq.tk /etc/nginx/sites-enabled/
    rm -rf /etc/nginx/sites-enabled/default
    sudo /bin/systemctl daemon-reload
    sudo /bin/systemctl enable chatbot-api.service
    echo "[+] Configured"
}

create_users() {
    if [ -z "`getent group chatbot_api`" ]; then
        addgroup --system chatbot_api >/dev/null 2>&1
    fi
    if [ -z "`getent passwd chatbot_api`" ]; then
        adduser --system --home /usr/lib/chatbot/api/.home --shell /bin/false \
			 --ingroup chatbot_api --disabled-password --disabled-login \
			 --gecos "Chatbot API account" chatbot_api >/dev/null 2>&1
    fi
    echo "[+] Created users"
}

set_permissions() {
    chown -R khaitq:khaitq /var/log/chatbot/api
    chmod 700 /var/log/chatbot/api
    
    chown root:khaitq /usr/lib/chatbot/api
    chmod 750 /etc/chatbot/api
    
    chown root:khaitq /usr/lib/chatbot/api
    chmod 750 /usr/lib/chatbot/api

    echo "[+] Set permissions"
}

start_services() {
    service chatbot-api restart
    service nginx reload
    echo "[+] Started services"
}

install_packages
make_dir
install_python_libs
replace
copy_source_code
config
# create_users
set_permissions
start_services









