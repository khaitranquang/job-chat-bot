import os
import traceback


BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


try:
    SECRET_KEY = '3fgk8255(eghshbe%-j976kucrc@v%)apbw@gep2ve7vpj4f$i'
    DEBUG = True
    APPEND_SLASH = False
    WSGI_APPLICATION = 'chatbot.wsgi.application'
    ALLOWED_HOSTS = ["*"]

    # DATABASES = {
    #     'default': {
    #         'ENGINE': "django.db.backends.mysql",
    #         'NAME': "bugbounty",
    #         'USER': "bugbounty",
    #         'PASSWORD': "1b2099b89dc2b3175c6cb56d76cdde52",
    #         'HOST': "mysql.db.platform.cystack.org",
    #         'PORT': 3306,
    #         'CONN_MAX_AGE': 600,
    #         'OPTIONS': {
    #             'init_command': 'ALTER DATABASE bugbounty CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci',
    #             'charset': 'utf8mb4',  # <--- Use this
    #         }
    #     }
    # }

    INSTALLED_APPS = [
        'django.contrib.auth',
        'django.contrib.contenttypes',
        'django.contrib.staticfiles',
        'corsheaders',
        'rest_framework',
        'v1_0',
    ]

    AUTHENTICATION_BACKENDS = [
        'django.contrib.auth.backends.AllowAllUsersModelBackend'
    ]

    MIDDLEWARE = [
        'corsheaders.middleware.CorsMiddleware',
        'django.middleware.common.CommonMiddleware',
        'shared.middlewares.response_error.ResponseErrorMiddleware',
    ]

    ROOT_URLCONF = 'chatbot.urls'

    REST_FRAMEWORK = {
        'DEFAULT_AUTHENTICATION_CLASSES': [
            # 'shared.authentications.basic_token_auth.BasicTokenAuthentication'
        ],
        'DEFAULT_RENDERER_CLASSES': (
            'rest_framework.renderers.JSONRenderer',
            # 'rest_framework.renderers.BrowsableAPIRenderer',
        ),
        'EXCEPTION_HANDLER': 'v1_0.apps.custom_exception_handler',
        'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.PageNumberPagination',
        'PAGE_SIZE': 10,

        'DEFAULT_THROTTLE_CLASSES': (
            'rest_framework.throttling.AnonRateThrottle',
            'rest_framework.throttling.UserRateThrottle'
        ),
        'DEFAULT_THROTTLE_RATES': {
            'anon': '1200/min',
            'user': '3000/min'
        }
    }

    CORS_ORIGIN_ALLOW_ALL = True
    CORS_ALLOW_CREDENTIALS = True
    CORS_EXPOSE_HEADERS = ('location', 'Location')
    CORS_ALLOW_HEADERS = (
        'accept',
        'accept-encoding',
        'authorization',
        'content-type',
        'dnt',
        'origin',
        'user-agent',
        'x-csrftoken',
        'x-requested-with',
        'username', 'password'
    )
    CORS_ALLOW_METHODS = (
        'GET',
        'POST',
        'PUT',
        'DELETE',
        'OPTIONS'
    )

    LANGUAGE_CODE = 'en-us'
    TIME_ZONE = 'UTC'
    USE_I18N = True
    USE_L10N = True
    USE_TZ = True
    STATIC_URL = '/static/'

except Exception as e:
    tb = traceback.format_exc()
    print(tb)
