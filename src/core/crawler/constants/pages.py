# Base urls
BASE_URL_CAREERLINK = "https://www.careerlink.vn/vieclam/list?view=headline"
BASE_URL_MYWORK = "https://mywork.com.vn/tuyen-dung"
BASE_URL_VIECLAM24H = "https://vieclam24h.vn/tim-kiem-viec-lam-nhanh"


# Advance crawler type
CRAWLER_TYPE_CAREER = "by_career"
CRAWLER_TYPE_LOCATION = "by_location"
CRAWLER_TYPE_SALARY = "by_salary"
