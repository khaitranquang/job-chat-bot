import re

from core.crawler.constants.pages import *


class BaseCrawler:
    def __init__(self, url, career=None, location=None, salary=None, **metadata):
        self._base_url = url
        self._career = career
        self._location = location
        self._salary = salary
        self._metadata = metadata

    @staticmethod
    def normalize_text(text):
        return re.sub(r"\s+", " ", text).strip()

    def simple_crawler(self):
        raise NotImplementedError

    def advance_crawler(self, type):
        if type == CRAWLER_TYPE_CAREER:
            return
        elif type == CRAWLER_TYPE_LOCATION:
            return
        elif type == CRAWLER_TYPE_SALARY:
            return

    def crawler_by_career(self):
        raise NotImplementedError

    def crawler_by_location(self):
        raise NotImplementedError

    def crawler_by_salary(self):
        raise NotImplementedError
