import requests
from bs4 import BeautifulSoup

from core.crawler.pages.base_page import BASE_URL_CAREERLINK
from core.crawler.pages.base_page import BaseCrawler


class CareerLinkCrawler(BaseCrawler):
    def __init__(self, career=None, location=None, salary=None, **metadata):
        url = BASE_URL_CAREERLINK
        super(CareerLinkCrawler, self).__init__(url, career, location, salary, **metadata)

    def simple_crawler(self, url=None):
        results = []
        if url is None:
            url = self._base_url
        res = requests.get(url=url)
        soup = BeautifulSoup(res.text, "html.parser")

        results_row = soup.find_all('div', {'class': 'list-group-item'})
        for row in results_row:
            # Get h2 tag
            h2 = row.find('h2')
            a_tag = h2.find('a', href=True)
            title = h2.text         # Get title (h2 text)
            url = "https://www.careerlink.vn" + a_tag['href']     # Get link in a_tag

            # Get org
            item_text = row.find('div', {'class': 'list-group-item-text'})
            priority_data = item_text.find('p', {'class': 'priority-data'})
            priority_a_tags = priority_data.find_all('a')
            org = priority_a_tags[0].text
            # Get location
            location = priority_a_tags[1].text

            # Get salary
            salary = item_text.find('div', {'class': 'pull-left'}).find('small').text

            # Get updated time
            updated_time = item_text.find('p', {'class': 'pull-right'}).find('small').text

            results.append({
                "title": self.normalize_text(text=title),
                "organization": org,
                "salary": self.normalize_text(text=salary),
                "location": location,
                "link": url,
                "expired_time": None,
                "updated_time": updated_time
            })
        return results

    def crawler_by_career(self):
        pass

    def crawler_by_location(self):
        pass

    def crawler_by_salary(self):
        pass


