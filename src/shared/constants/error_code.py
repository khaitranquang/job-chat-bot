APP_CODE = [
    {
        "code": "0000",
        "message": "The authentication token is invalid",
        "vi_message": "Token đăng nhập không hợp lệ, gộp chung các trường hợp như token hết hạn, không decode được...v"
    },
    {
        "code": "0002",
        "message": "The account does not have enough permission to execute this operation",
        "vi_message": "Không đủ quyền để thực hiện chức năng này"
    },
    {
        "code": "0004",
        "message": "Invalid data",
        "vi_message": "Dữ liệu không hợp lệ"
    },
    {
        "code": "0005",
        "message": "Method Not Allowed",
        "vi_message": "Phương thức truy cập không hợp lệ"
    },
    {
        "code": "0008",
        "message": "Unknown Error",
        "vi_message": "Khong biet loi"
    }
]


def get_app_code_content(code):
    try:
        return [content for content in APP_CODE if content["code"] == code][0]['message']
    except:
        raise Exception("Does not have this app_code")
