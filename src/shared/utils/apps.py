from datetime import datetime
import pytz
from django.core.validators import URLValidator
from django.core.exceptions import ValidationError


def now():
    """
    Get timestamp now
    :return: now (int)
    """
    time_now = datetime.now(tz=pytz.UTC)
    timestamp_now = time_now.timestamp()
    return int(timestamp_now)


def diff_list(new_list, old_list):
    return [x for x in new_list if x not in old_list]


def check_url(url):
    """
    Check string is valid url
    :param url: String need check
    :return: True if valid
    """
    url_validator = URLValidator(schemes=['http', 'https'])
    try:
        url_validator(url)
        return True
    except ValidationError as e:
        return False
