from django.conf.urls import url, include
from rest_framework.routers import DefaultRouter

from v1_0 import views


router = DefaultRouter(trailing_slash=False)
router.register(r'crawler', views.CrawlerViewSet, 'crawler')

urlpatterns = [
    url(r'^', include(router.urls)),
]
