from rest_framework.viewsets import GenericViewSet
from rest_framework import mixins


class ChatbotBaseView(mixins.CreateModelMixin,
                   mixins.UpdateModelMixin,
                   mixins.RetrieveModelMixin,
                   mixins.ListModelMixin,
                   mixins.DestroyModelMixin,
                   GenericViewSet):
    def initial(self, request, *args, **kwargs):
        super(ChatbotBaseView, self).initial(request, *args, **kwargs)

        # Something here
        # ...

