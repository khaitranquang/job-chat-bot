from rest_framework.response import Response
from rest_framework.exceptions import NotFound, ValidationError
from rest_framework.decorators import action
from rest_framework.permissions import AllowAny

from core.crawler.pages.careerlink import CareerLinkCrawler

from v1_0.views.app_general_view import ChatbotBaseView


class CrawlerViewSet(ChatbotBaseView):
    permission_classes = (AllowAny, )
    http_method_names = ["head", "options", "get"]

    def get_permissions(self):
        return super(CrawlerViewSet, self).get_permissions()

    @action(methods=["get"], detail=False)
    def career(self, request, **data):
        results = CareerLinkCrawler().simple_crawler()
        return Response(status=200, data=results)
